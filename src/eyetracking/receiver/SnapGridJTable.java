/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eyetracking.receiver;


/**
 *
 * @author nsilva
 */
public class SnapGridJTable {
    //Plots Selection
    private int startX;
    private int startY;
    private int row = 0;
    private int column = 0;
    
    //Fixation Durations
    private double duration = 0.0;
    private double totalDuration = 0.0;

    //Fixation Count
    private long fixationCount = 0;
    
    //Lins Results
    private int id = 0;
    private String path = "";
    private String dim1 = "";
    private String dim2 = "";    
    private float distanceSimilarity;
    private float[] featurevector;  
    
    private int detectedX = -1;
    private int detectedY = -1;      

    public SnapGridJTable() { }
    
    public int getStartX() { return startX; }

    public void setStartX(int startX) { this.startX = startX; }
     
    public int getStartY() { return startY; }

    public void setStartY(int startY) { this.startY = startY; }

    public double getDuration() { return duration; }

    public void setDuration(double duration) { this.duration = duration; }

    public double getTotalDuration() { return totalDuration; }

    public void setTotalDuration(double totalDuration) { this.totalDuration = totalDuration; }

    public long getFixationCount() { return fixationCount; }

    public void setFixationCount(long fixationCount) { this.fixationCount = fixationCount; }
    
    public void addFixation(int count) { this.fixationCount += count; }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getPath() { return path; }

    public void setPath(String path) { this.path = path; }

    public String getDim1() { return dim1; }

    public void setDim1(String dim1) { this.dim1 = dim1; }

    public String getDim2() { return dim2; }

    public void setDim2(String dim2) { this.dim2 = dim2; }

    public float getDistanceSimarity() { return distanceSimilarity; }

    public void setDistanceSimilarity(float distance) { this.distanceSimilarity = distance; }

    public float[] getFeaturevector() { return featurevector; }

    public void setFeaturevector(float[] featurevector) { this.featurevector = featurevector; }    

    public int getRow() { return row; }

    public void setRow(int row) { this.row = row; }

    public int getColumn() { return column; }

    public void setColumn(int column) { this.column = column; }
    
    public int getDetectedX() { return detectedX; }

    public void setDetectedX(int detectedX) { this.detectedX = detectedX; }

    public int getDetectedY() { return detectedY; }

    public void setDetectedY(int detectedY) { this.detectedY = detectedY; }
}