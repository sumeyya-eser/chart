/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eyetracking.receiver;

import com.theeyetribe.clientsdk.IGazeListener;
import com.theeyetribe.clientsdk.data.GazeData;

import eyetracking.receiver.EyeTribeCursor;
import eyetracking.receiver.SnapGridJTable;

import eyetracking.heatmap.ImagePainter;

import gui.MainWindow;

/**
 * @author Futshi
 * GazeListener (class)
 * - IGazeListener but override update function, which is triggered
 *   frequently and returns gazeData (eye tracking data)
 * TODO
 * - add grid tracking & computation
 *   (WIP, onGazeUpdate implementation still missing)
*/
public class GazeListener implements IGazeListener {
    private MainWindow mainWindow;
    private EyeTribeCursor eyeTribeCursor;
    private ImagePainter imagePainter;
    
    private int currentAOIRow = -1;
    private int currentAOIColumn = -1;
    private int prevAOIRow = -1;
    private int prevAOIColumn = -1;
    private int pnlWidth;
    private int pnlHeight;
    private int gridSize = 6;
    // grid-offset from top-left-corner of screen (TODO dynamic value generation)
    private int startX = 320;
    private int startY = 55;
    private SnapGridJTable[][] snapGrid;
    
    /**
     * constructor
     * @param mainWindow ... MainWindow component
     */
    public GazeListener(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        // image painter neede for heatmap :)
        imagePainter = new ImagePainter(mainWindow.getWidth(), mainWindow.getHeight());
        mainWindow.setHeatmapImagePainter(imagePainter);
        // eye tribe cursor :)
        eyeTribeCursor = new EyeTribeCursor();
        eyeTribeCursor.setHeightGap(24);
        mainWindow.setEyeTribeCursor(eyeTribeCursor);
        // grid initialization
        pnlWidth = mainWindow.getExplorationPnlWidth();
        pnlHeight = mainWindow.getExplorationPnlHeight();
        snapGrid = new SnapGridJTable[gridSize][gridSize];
        int columnWidth = pnlWidth / gridSize;
        int rowHeight = pnlHeight / gridSize;
        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                snapGrid[i][j] = new SnapGridJTable();
                snapGrid[i][j].setStartX(startX + columnWidth * j);
                snapGrid[i][j].setStartY(startY + rowHeight * i);
            }
        }
    }
    
    /**
     * function handling listener update in intervals
     * @param gazeData ... data sent from device
     */
    @Override
    public void onGazeUpdate(GazeData gazeData) {
        int smoothX = (int)gazeData.smoothedCoordinates.x;
        int smoothY = (int)gazeData.smoothedCoordinates.y;
        int rawX = (int)gazeData.rawCoordinates.x;
        int rawY = (int)gazeData.rawCoordinates.y;
        // log
        System.out.println("gazeData.smoothedCoordinates: [" + smoothX + "," + smoothY + "]");
        System.out.println("gazeData.rawCoordinates: [" + rawX + "," + rawY + "]");
        //System.out.println("gazeData.isFixated: " + gazeData.isFixated);
        // cursor
        eyeTribeCursor.moveSquare((int)gazeData.smoothedCoordinates.x, (int)gazeData.smoothedCoordinates.y);
        // heatmap
        if(mainWindow.isHeatmapActive()) { imagePainter.updateImage2(rawX, rawY); }
        // grid (AOI = area of interest)
        double durationPrevAOI = 0;
        if (prevAOIRow != -1 && prevAOIColumn != -1) {
            //calculates total time for previous AOI (System.nanoTime() yields current time)
            durationPrevAOI = (System.nanoTime() - snapGrid[prevAOIRow][prevAOIColumn].getDuration()) / 1e6;
        }
        boolean gridHit = false;
        if(smoothX > startX && smoothX < pnlWidth + startX && smoothY > startY && smoothY < pnlHeight + startY) {
            gridHit = true;
            // find exact grid
            for(int j = 1; j < snapGrid[0].length; j++) {
                if(smoothX < snapGrid[0][j].getStartX()) {
                    currentAOIColumn = j - 1;
                    break;
                } else {
                    currentAOIColumn = snapGrid[0].length - 1;
                }
            }
            for(int i = 0; i < snapGrid.length; i++) {
                if(smoothX < snapGrid[i][0].getStartY()) {
                    currentAOIRow = i - 1;
                    break;
                } else {
                    currentAOIRow = snapGrid.length - 1;
                }
            }
        } else {
            currentAOIRow = -1;
            currentAOIColumn = -1;
        }
        if(gridHit) {
            int lensX = smoothX - startX;
            int lensY = smoothY - startY;
            mainWindow.setLensEyePosition(lensX, lensY);
            if(currentAOIRow != -1 && currentAOIColumn != -1) {
                snapGrid[currentAOIRow][currentAOIColumn].setDuration(System.nanoTime());
                if(prevAOIRow != -1 && prevAOIColumn != -1) {
                    // previous AOI valid
                    mainWindow.updateDuration(prevAOIRow, prevAOIColumn, durationPrevAOI);
                } else if(prevAOIRow != currentAOIRow || prevAOIColumn != currentAOIColumn) {
                    // previous AOI invalid
                    mainWindow.updateFixation(currentAOIRow, currentAOIColumn);
                    mainWindow.logInfo(currentAOIRow, currentAOIColumn);
                }
            }
        } else {
            if(prevAOIRow != -1) {
                mainWindow.updateDuration(prevAOIRow, prevAOIColumn, durationPrevAOI);
                mainWindow.unselectAOIs(prevAOIRow, prevAOIColumn);
                mainWindow.logInfo(currentAOIRow, currentAOIColumn);
            }
        }
        prevAOIColumn = currentAOIColumn;
        prevAOIRow = currentAOIRow;
    }
}
