package eyetracking.receiver;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * @author Lin Shao, Jia Jun Yan
 */
public class EyeTribeCursor extends JPanel {
    private int squareX = 0;
    private int squareY = 0;
    private int squareW = 10;
    private int squareH = 10;
    
    private int widthGap = 0;
    private int heightGap = 0; //top bar
    
    private Color cursorColor = Color.RED;
    private boolean enabled = false;

    /**
     * constructor
     */
    public EyeTribeCursor() {
        setBorder(BorderFactory.createLineBorder(Color.black));
    }
    
    public void moveSquare(int xt, int yt) {
        int OFFSET = 1;
        int x = xt - widthGap;
        int y = yt - heightGap;
        
        if ((squareX!=x) || (squareY!=y)) {
            this.repaint(squareX,squareY,squareW+OFFSET,squareH+OFFSET);
            squareX=x;
            squareY=y;
            this.repaint(squareX,squareY,squareW+OFFSET,squareH+OFFSET);                       
        } 
    }
 
    public Dimension getPreferredSize() {
        return new Dimension(250,200);
    }
    
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);    
        super.setOpaque(false);
        g.setColor(cursorColor);
        g.fillOval(squareX, squareY, squareW, squareH);
//        g.fillRect(squareX,squareY,squareW,squareH);
        g.setColor(Color.BLACK);
        g.drawOval(squareX,squareY,squareW,squareH);
//        g.drawRect(squareX,squareY,squareW,squareH);
    }  

    /**
     * @param widthGap the widthGap to set
     */
    public void setWidthGap(int widthGap) { this.widthGap = widthGap; }

    /**
     * @param heightGap the heightGap to set
     */
    public void setHeightGap(int heightGap) { this.heightGap = heightGap; }

    /**
     * @return the enable
     */
    public boolean isEnabled() { return enabled; }

    /**
     * @param enable the enable to set
     */
    public void setEnabled(boolean enabled) { this.enabled = enabled; }
    
    public void changeCursorColor(Color c){ this.cursorColor = c; }

}