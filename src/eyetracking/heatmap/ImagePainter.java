package eyetracking.heatmap;

import java.awt.Point;
import java.awt.image.BufferedImage;

public class ImagePainter {
    final int AREA = 20;
    
    private BufferedHeatmapImage image;
    private int intensity = 8; /* Intensity from 1 to 10 */
    
    /**
     * Creates image with dimension of screen, and fills it with the starting color.
     */
    public ImagePainter(int w, int h){
        image = new BufferedHeatmapImage(w, h);
    }

    public void resetImage() {
        image.setInitialColor();
    }

    public void updateImage(int gX, int gY) {
        //Coordinate [0,0] usually means eye-tracker couldn't track the eyes
        if (gX == 0 && gY == 0) { return; }
        int w = image.getWidth();
        int h = image.getHeight();
        //Check bounds
        if (gX < 0 || gX > w || gY < 0 || gY > h) { return; }
        for (int x = -AREA; x <= AREA; x++) {
            if (gX + x < 0) { continue; }
            if (gX + x > w) { break; }
            for (int y = -AREA; y <= AREA; y++) {
                if (gY + y < 0) { continue; }
                if (gY + y > h) { break; }
                image.updatePixel(gX + x, gY + y, intensity);
            }
        }
    }
    
    public void updateImage2(int gX, int gY) {
        //Coordinate [0,0] usually means eye-tracker couldn't track the eyes
        if (gX == 0 && gY == 0) { return; }
        int w = image.getWidth();
        int h = image.getHeight();
        //Check bounds
        if (gX < 0 || gX > w || gY < 0 || gY > h) { return; }
        Point center = new Point(0, 0);
        int r = AREA;
        for(int x = -AREA - r; x <= AREA + r; x++){
            if (gX + x < 0) { continue; }
            if (gX + x > w) { break; }
            for(int y = -AREA - r; y <= AREA + r; y++){
                if (gY + y < 0) { continue; }
                if (gY + y > h) { break; }
                Point p = new Point(x,y);
                if(p.distance(center) <= r){
                    image.updatePixel(gX + x, gY + y, intensity);
                }
            }
        }
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }
}
