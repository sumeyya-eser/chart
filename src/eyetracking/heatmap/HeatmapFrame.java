package eyetracking.heatmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import java.awt.image.BufferedImage;

public class HeatmapFrame extends JPanel {
    
    private BufferedImage bufferedImage;
    
    public HeatmapFrame(final BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
        
        setBorder(BorderFactory.createLineBorder(Color.black));
        setOpaque(false);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(bufferedImage, 0, 0, null);

        try {
            Thread.sleep(1000/15);
            repaint();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
