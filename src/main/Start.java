package main;
// UI components
import javax.swing.JFrame;
// official eyetribe client sdk (replacement for eyetracking library above)
import com.theeyetribe.clientsdk.GazeManager;
import com.theeyetribe.clientsdk.IGazeListener;
import com.theeyetribe.clientsdk.data.GazeData;
import static com.theeyetribe.clientsdk.GazeManager.ApiVersion;
import static com.theeyetribe.clientsdk.GazeManager.ClientMode;
// custom gui
import gui.MainWindow;
// custom heatmap components
import eyetracking.heatmap.ImagePainter;
// custom eyetracker listener
import eyetracking.receiver.GazeListener;

/**
 * @author Lin Shao, Jia Jun Yan
 */
public class Start {
    static boolean eyeTrackerActivated = true;
    
    public static void main(String[] args) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        // </editor-fold>
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                // init gui
                MainWindow mainWindow = new MainWindow();
                mainWindow.setVisible(true);
                mainWindow.setExtendedState(mainWindow.getExtendedState() | JFrame.MAXIMIZED_BOTH);
                // init eyetracker
                if(eyeTrackerActivated) {
                    System.out.println("[Start.java, EyeTracker] initializing...");
                    final GazeManager gazeManager = GazeManager.getInstance();
                    final GazeListener gazeListener = new GazeListener(mainWindow);
                    gazeManager.addGazeListener(gazeListener);
                    Runtime.getRuntime().addShutdownHook(new Thread() {
                        @Override
                        public void run() {
                            gazeManager.removeGazeListener(gazeListener);
                            gazeManager.deactivate();
                        }
                    });
                    boolean success = gazeManager.activate(ApiVersion.VERSION_1_0, ClientMode.PUSH);
                    System.out.println("[Start.java, EyeTracker] device active: " + success);
                }
            }            
        });
    }    
}
