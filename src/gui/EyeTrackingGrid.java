/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Lin Shao
 */
public class EyeTrackingGrid extends JPanel {
    private int columnCount;
    private int rowCount;
    private double[][] cellDuration;
    
    public EyeTrackingGrid(int rows, int cols) {
        this.columnCount = cols;
        this.rowCount = rows;
        this.cellDuration = new double[cols][rows];
        
        this.setOpaque(false);
        this.setLayout(new GridLayout(rows, cols));
        
        Random rnd = new Random();
        Color[] colors = new Color[]{Color.GREEN, Color.BLUE, Color.RED, Color.MAGENTA};
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                JPanel cell = new JPanel();
                int color = rnd.nextInt(4);
                cell.setOpaque(false);
                cell.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                add(cell);
            }
        }
    }

    public void setTotalDuration(int preColumn, int preRow,  double duration) {
        cellDuration[preColumn][preRow] = cellDuration[preColumn][preRow] + duration;
        //System.out.println(preColumn + "-" + preRow +": " + cellDuration[preColumn][preRow]);
    }

    public double getTotalDuration(int preColumn, int preRow) {
        return cellDuration[preColumn][preRow];
    }
}
