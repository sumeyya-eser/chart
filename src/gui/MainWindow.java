package gui;

import javax.swing.JFileChooser;
import java.io.File;
import model.LensModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import model.FileModel;
import eyetracking.heatmap.HeatmapFrame;
import eyetracking.heatmap.ImagePainter;
import eyetracking.receiver.EyeTribeCursor;
import static gui.SortingVisualizer.frame;
import static gui.SortingVisualizer.resetArray;
import java.awt.EventQueue;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;
import org.jfree.ui.TextAnchor;

/**
 * @author Lin Shao
 */
public class MainWindow extends JFrame implements KeyListener {

    HistogramPanel pnl = new HistogramPanel();
    private SortingVisualizer sort;
    private FileModel model = null;
    private static ScatterPlot[][] spMatrix;
    private ArrayList<LensModel> lensHistory = new ArrayList<LensModel>();

    private ScatterPlot projection = null;

    protected ButtonPanel btnSwitchAxes = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnBothAxes = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnCrossVal = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnErrorColor = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnHistogram = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnLoadData = new ButtonPanel(new BorderLayout());

    protected ButtonPanel btnRegAuto = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnRegLin = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnRegQuad = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnRegCub = new ButtonPanel(new FlowLayout());
    protected ButtonPanel btnReg4 = new ButtonPanel(new FlowLayout());

    private int prevC = -1;
    private int prevR = -1;

    private double o = 0;
    private double eX = 0;
    private double eY = 0;

    private int bestModel = -1;
    private Color[] colorCorrelations = {new Color(255, 0, 0), new Color(255, 127, 0), new Color(0, 255, 0)};

    private EyeTrackingGrid eyeTrackingGrid;
    private EyeTribeCursor eyeTribeCursor;
    private boolean useEyePosition = false;

    private ImagePainter imagePainter;
    private boolean heatmapActive = false;
    private HeatmapFrame heatmapFrame;
    private String lastFolder = "C:\\Users\\lshao\\Desktop";

    public MainWindow() {
        initComponents();

        addKeyListener(this);
        setFocusable(true);
        createMenuBar();

        eyeTrackingGrid = new EyeTrackingGrid(6, 6);
        eyeTrackingGrid.setVisible(false);
        pnlOverlaySP.add(eyeTrackingGrid, Integer.valueOf(2));

        pnlClassLabels.setBackground(new Color(153, 153, 153));

        btnLoadData.setBackground(new Color(153, 153, 153));
        JLabel label = new JLabel("Load Data");
        label.setFont(new Font("Segore UI", Font.BOLD, 24));
        label.setForeground(Color.WHITE);
        label.setBorder(new EmptyBorder(5, 5, 0, 0));
        btnLoadData.add(label, BorderLayout.LINE_START);
        pnlLoadData.add(btnLoadData, BorderLayout.CENTER);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets(0, 0, 5, 0);  //top padding
        c.gridy = 0;

        pnlEyetracking1.setPreferredSize(new Dimension(294, 35));
        pnlEyetracking1.setBackground(new Color(240, 240, 240));
        JLabel lblEyetracking = new JLabel("Eye Tracking");
        lblEyetracking.setFont(new Font("Segore UI", Font.BOLD, 13));
        lblEyetracking.setBorder(new EmptyBorder(5, 0, 0, 0));
        pnlEyetracking1.add(lblEyetracking);

        pnlEyetracking2.setPreferredSize(new Dimension(294, 35));
        pnlEyetracking2.setBackground(new Color(240, 240, 240));
        JLabel lblEyetracking2 = new JLabel("Show Heatmap");
        lblEyetracking2.setFont(new Font("Segore UI", Font.BOLD, 13));
        lblEyetracking2.setBorder(new EmptyBorder(5, 0, 0, 0));
        pnlEyetracking2.add(lblEyetracking2);

        pnlEyetracking3.setPreferredSize(new Dimension(294, 35));
        pnlEyetracking3.setBackground(new Color(240, 240, 240));
        JLabel lblEyetracking3 = new JLabel("Heatmap Tracking");
        lblEyetracking3.setFont(new Font("Segore UI", Font.BOLD, 13));
        lblEyetracking3.setBorder(new EmptyBorder(5, 0, 0, 0));
        pnlEyetracking3.add(lblEyetracking3);

        pnlEyetracking4.setPreferredSize(new Dimension(294, 35));
        pnlEyetracking4.setBackground(new Color(240, 240, 240));
        JLabel lblEyetracking4 = new JLabel("Reset Heatmap");
        lblEyetracking4.setFont(new Font("Segore UI", Font.BOLD, 13));
        lblEyetracking4.setBorder(new EmptyBorder(5, 0, 0, 0));
        pnlEyetracking4.add(lblEyetracking4);
        
       
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("./img/lens.png"));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlOverlay = new javax.swing.JLayeredPane();
        pnlMain = new javax.swing.JPanel();
        pnlSettingA = new javax.swing.JPanel();
        pnlClasses1 = new javax.swing.JPanel();
        lblFilter1 = new javax.swing.JLabel();
        scrClassLabels = new javax.swing.JScrollPane();
        pnlClassLabels = new javax.swing.JPanel();
        cxbxClassColor = new javax.swing.JCheckBox();
        pnlEyeTracking = new javax.swing.JPanel();
        lblRegTypes2 = new javax.swing.JLabel();
        pnlEyetracking1 = new gui.ButtonPanel(new FlowLayout());
        pnlEyetracking2 = new gui.ButtonPanel(new FlowLayout());
        pnlEyetracking3 = new gui.ButtonPanel(new FlowLayout());
        pnlEyetracking4 = new gui.ButtonPanel(new FlowLayout());
        pnlLoadData = new javax.swing.JPanel();
        pnlCenter = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        pnlSPLOM2 = new javax.swing.JPanel();
        pnlSort = new javax.swing.JPanel();
        pnlChart = new javax.swing.JPanel();
        pnlOverlaySP = new javax.swing.JLayeredPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1585, 947));

        pnlOverlay.setLayout(new javax.swing.OverlayLayout(pnlOverlay));

        pnlMain.setLayout(new java.awt.BorderLayout());

        pnlSettingA.setBackground(new java.awt.Color(0, 0, 0));
        pnlSettingA.setPreferredSize(new java.awt.Dimension(300, 1110));
        pnlSettingA.setVerifyInputWhenFocusTarget(false);
        pnlSettingA.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlClasses1.setBackground(new java.awt.Color(153, 153, 153));

        lblFilter1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblFilter1.setForeground(new java.awt.Color(255, 255, 255));
        lblFilter1.setText("Class Labels");

        scrClassLabels.setBorder(null);

        pnlClassLabels.setLayout(new java.awt.GridBagLayout());
        scrClassLabels.setViewportView(pnlClassLabels);

        cxbxClassColor.setBackground(new java.awt.Color(153, 153, 153));
        cxbxClassColor.setSelected(true);
        cxbxClassColor.setText("Color-Code");
        cxbxClassColor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cxbxClassColorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlClasses1Layout = new javax.swing.GroupLayout(pnlClasses1);
        pnlClasses1.setLayout(pnlClasses1Layout);
        pnlClasses1Layout.setHorizontalGroup(
            pnlClasses1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlClasses1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cxbxClassColor)
                .addContainerGap())
            .addComponent(scrClassLabels)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlClasses1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblFilter1, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
        );
        pnlClasses1Layout.setVerticalGroup(
            pnlClasses1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlClasses1Layout.createSequentialGroup()
                .addComponent(lblFilter1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrClassLabels, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cxbxClassColor)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlSettingA.add(pnlClasses1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 130, 300, 200));

        pnlEyeTracking.setBackground(new java.awt.Color(153, 153, 153));

        lblRegTypes2.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblRegTypes2.setForeground(new java.awt.Color(255, 255, 255));
        lblRegTypes2.setText("Eye Tracking");

        javax.swing.GroupLayout pnlEyeTrackingLayout = new javax.swing.GroupLayout(pnlEyeTracking);
        pnlEyeTracking.setLayout(pnlEyeTrackingLayout);
        pnlEyeTrackingLayout.setHorizontalGroup(
            pnlEyeTrackingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEyeTrackingLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblRegTypes2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(pnlEyetracking1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlEyetracking2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlEyetracking3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlEyeTrackingLayout.createSequentialGroup()
                .addComponent(pnlEyetracking4, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlEyeTrackingLayout.setVerticalGroup(
            pnlEyeTrackingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEyeTrackingLayout.createSequentialGroup()
                .addComponent(lblRegTypes2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlEyetracking1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlEyetracking2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(pnlEyetracking3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlEyetracking4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlSettingA.add(pnlEyeTracking, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 380, 300, 220));

        pnlLoadData.setBackground(new java.awt.Color(153, 153, 153));
        pnlLoadData.setLayout(new java.awt.BorderLayout());
        pnlSettingA.add(pnlLoadData, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 20, 300, 40));

        pnlMain.add(pnlSettingA, java.awt.BorderLayout.WEST);

        pnlCenter.setBackground(new java.awt.Color(204, 204, 204));
        pnlCenter.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlCenter.setEnabled(false);
        pnlCenter.setPreferredSize(new java.awt.Dimension(1200, 557));
        pnlCenter.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("Sorting Button");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        pnlCenter.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, -1, -1));

        jButton2.setText("Bar Chart");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        pnlCenter.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        pnlSPLOM2.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout pnlSPLOM2Layout = new javax.swing.GroupLayout(pnlSPLOM2);
        pnlSPLOM2.setLayout(pnlSPLOM2Layout);
        pnlSPLOM2Layout.setHorizontalGroup(
            pnlSPLOM2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 2196, Short.MAX_VALUE)
        );
        pnlSPLOM2Layout.setVerticalGroup(
            pnlSPLOM2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1222, Short.MAX_VALUE)
        );

        pnlCenter.add(pnlSPLOM2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1700, 90, -1, -1));

        pnlSort.setBackground(new java.awt.Color(0, 0, 0));
        pnlSort.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlSort.setForeground(new java.awt.Color(255, 255, 255));
        pnlSort.setAlignmentX(1.5F);
        pnlSort.setAlignmentY(0.35F);
        pnlSort.setMaximumSize(new java.awt.Dimension(1000, 1000));
        pnlSort.setMinimumSize(new java.awt.Dimension(1, 1));
        pnlSort.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlSort.setLayout(new java.awt.BorderLayout());
        pnlCenter.add(pnlSort, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 890, 710));

        pnlChart.setBackground(new java.awt.Color(0, 0, 0));
        pnlChart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlChart.setForeground(new java.awt.Color(255, 255, 255));
        pnlChart.setAlignmentX(1.5F);
        pnlChart.setAlignmentY(0.35F);
        pnlChart.setMaximumSize(new java.awt.Dimension(1000, 1000));
        pnlChart.setMinimumSize(new java.awt.Dimension(1, 1));
        pnlChart.setPreferredSize(new java.awt.Dimension(1, 1));
        pnlChart.setLayout(new java.awt.BorderLayout());
        pnlCenter.add(pnlChart, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 780, 710));

        pnlOverlaySP.setLayout(new javax.swing.OverlayLayout(pnlOverlaySP));
        pnlCenter.add(pnlOverlaySP, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 38, 1700, 1270));

        pnlMain.add(pnlCenter, java.awt.BorderLayout.CENTER);

        pnlOverlay.add(pnlMain);

        getContentPane().add(pnlOverlay, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("View");
        menuBar.add(menu);

        JMenuItem menuItem = new JMenuItem("Show Heatmap");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                toggleHeatmapVisibility();
            }
        });
        menu.add(menuItem);

        JMenuItem menuItem2 = new JMenuItem("Heatmap Tracking");
        menuItem2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                toggleHeatmapTracking();
            }
        });
        menu.add(menuItem2);

        JMenuItem menuItem3 = new JMenuItem("Reset Heatmap");
        menuItem3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                imagePainter.resetImage();
            }
        });
        menu.add(menuItem3);

        JMenuItem menuItem4 = new JMenuItem("Save Heatmap");
        menuItem4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveHeatmap();
            }
        });
        menu.add(menuItem4);

        setJMenuBar(menuBar);
    }

    public void setEyeTribeCursor(EyeTribeCursor eyeTribeCursor) {
        this.eyeTribeCursor = eyeTribeCursor;
        pnlOverlay.add(eyeTribeCursor, Integer.valueOf(2));
    }

    public void setHeatmapImagePainter(ImagePainter imgPainter) {
        this.imagePainter = imgPainter;
        BufferedImage bufferedImage = imagePainter.getImage();
        heatmapFrame = new HeatmapFrame(bufferedImage);
        heatmapFrame.setVisible(false);
        pnlOverlay.add(heatmapFrame, Integer.valueOf(2));
    }

    public void toggleHeatmapVisibility() {
        heatmapFrame.setVisible(!heatmapFrame.isVisible());
    }

    public void toggleHeatmapTracking() {
        heatmapActive = !heatmapActive;
    }

    public void resetHeatmap() {
        imagePainter.resetImage();
    }

    public boolean isHeatmapActive() {
        return heatmapActive;
    }

    private void cxbxClassColorActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cxbxClassColorActionPerformed
    {//GEN-HEADEREND:event_cxbxClassColorActionPerformed
        setColorCode(cxbxClassColor.isSelected());
        projection.updateUI();
    }//GEN-LAST:event_cxbxClassColorActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        frame = new VisualizationPanel();
        resetArray();
        pnlSort.add(frame);
        pnlSort.setVisible(true);
        
    }//GEN-LAST:event_jButton1ActionPerformed

//BarChart
    private CategoryDataset createDataset() {
        String row = "Row";
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(350, row, "97");
        dataset.addValue(450, row, "98");
        dataset.addValue(500, row, "99");
        dataset.addValue(340, row, "00");
        dataset.addValue(180, row, "01");
        dataset.addValue(550, row, "02");
        dataset.addValue(450, row, "03");
        dataset.addValue(600, row, "04");
        dataset.addValue(750, row, "05");
        dataset.addValue(570, row, "06");
        dataset.addValue(360, row, "07");
        dataset.addValue(504, row, "08");
        dataset.addValue(350, row, "09");
        dataset.addValue(690, row, "10");
        dataset.addValue(510, row, "11");
        dataset.addValue(640, row, "12");
        dataset.addValue(250, row, "13");
        dataset.addValue(180, row, "14");
        dataset.addValue(365, row, "15");
        dataset.addValue(850, row, "16");
        dataset.addValue(560, row, "17");
        dataset.addValue(765, row, "18");
        dataset.addValue(680, row, "19");
        dataset.addValue(550, row, "20");
        return dataset;
    }

    private JFreeChart createChart(CategoryDataset dataset) {
        CategoryAxis categoryAxis = new CategoryAxis("");
        ValueAxis valueAxis = new NumberAxis("");
        valueAxis.setVisible(false);
        BarRenderer renderer = new BarRenderer() {

            @Override
            public Paint getItemPaint(int row, int column) {
                return Color.blue;
//                switch (column) {
//                    case 0:
//                        return Color.red;
//                    case 1:
//                        return Color.yellow;
//                    case 2:
//                        return Color.blue;
//                    case 3:
//                        return Color.orange;
//                    case 4:
//                        return Color.gray;
//                    case 5:
//                        return Color.green.darker();
//                    default:
//                        return Color.red;
//                }
            }
        };
        renderer.setDrawBarOutline(false);
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(
                ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER));
        renderer.setBaseItemLabelsVisible(Boolean.TRUE);
        renderer.setBarPainter(new StandardBarPainter());
        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
        JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        chart.setBackgroundPaint(Color.white);
        return chart;
    }

    private void display() {

        pnlChart.add(new ChartPanel(createChart(createDataset())));
        pnlChart.setVisible(true);
  
    }
//Bar Chart Finish

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        EventQueue.invokeLater(() -> {
            display();
        });

    }//GEN-LAST:event_jButton2ActionPerformed

    public void loadData() {
        JFileChooser chooser = new JFileChooser("./data");
        int returnVal = chooser.showOpenDialog(null);

        if (returnVal == chooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            model = new FileModel(file.getAbsolutePath(), ";", true); //normalize = false/true

            ArrayList<Object>[] data = model.getDataSet();
            ArrayList<String> labels = new ArrayList<String>();
            ArrayList<Integer> classIDs = new ArrayList<Integer>();

            if (!model.getClassIndex().isEmpty()) {
                int classIndex = model.getClassIndex().get(0);

                for (int i = 0; i < data[classIndex].size(); i++) {
                    if (!labels.contains(data[classIndex].get(i))) {
                        labels.add(String.valueOf(data[classIndex].get(i)));
                    }
                    int index = labels.indexOf(data[classIndex].get(i));
                    classIDs.add(index);
                }
                model.setClassNames(labels);
                model.setClasses2Points(classIDs);
            }

            //createSPLOM();
            // Create Class Label List
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = 0;
            c.insets = new Insets(0, 0, 5, 0);

            pnlClassLabels.removeAll();
            for (int i = 0; i < model.getClassNames().size(); i++) {
                ButtonPanel pnl = new ButtonPanel(new FlowLayout());
                pnl.setPreferredSize(new Dimension(294, 35));
                pnl.setBackground(new Color(240, 240, 240));
                JLabel label = new JLabel(model.getClassNames().get(i));
                label.setFont(new Font("Segore UI", Font.BOLD, 13));
                label.setForeground(Color.BLACK);
                label.setBorder(new EmptyBorder(5, 0, 0, 0));
                pnl.add(label);
                pnl.setName(label.getText());
                pnl.setSelected(true);
                c.gridy = i;
                pnlClassLabels.add(pnl, c);
            }
            pnlCenter.updateUI();

        } else {
            System.out.println("Open command cancelled by user.");
        }
    }

    public void filterForClass() {
        ArrayList<Boolean> classFilter = new ArrayList<Boolean>();
        for (int i = 0; i < pnlClassLabels.getComponents().length; i++) {
            ButtonPanel btnDim = (ButtonPanel) pnlClassLabels.getComponents()[i];
            if (btnDim.isSelected()) {
                classFilter.add(true);
            } else {
                classFilter.add(false);
            }
        }
        projection.changeFilter(classFilter);
    }

//    private void createSPLOM() {
//        ArrayList<String> header = model.getDataHeader();
//        int nrAttributes = header.size() - model.getNrOfCatDims();
//        ArrayList<Double[]> data = model.getNumDataSet();
//
//        int panelWidth = pnlSPLOM2.getWidth() - 30;
//        int panelHeight = pnlSPLOM2.getHeight();
//        int cellWidth = panelWidth / nrAttributes; //header.size();
//        int cellHeight = panelHeight / nrAttributes; //header.size();
//
//        /* Create SPLOM */
//        pnlSPLOM2.setLayout(new GridLayout(nrAttributes, nrAttributes));
//        spMatrix = new ScatterPlot[nrAttributes][nrAttributes];
//        for (int y = 0; y < nrAttributes; y++) {
//            for (int x = 0; x < nrAttributes; x++) {
//                if (x == y) {
//                    JPanel jp = new JPanel(new BorderLayout());
//                    jp.setBorder(BorderFactory.createLineBorder(Color.black));
//                    jp.setBackground(Color.BLACK);
//
//                    JLabel text2 = new JLabel("" + header.get(x), JLabel.CENTER);
//                    text2.setFont(new Font("Verdana", Font.BOLD, 12));
//                    text2.setForeground(Color.red);
//                    jp.add(text2, BorderLayout.CENTER);
//                    pnlSPLOM2.add(jp);
//                } else {
//                    ArrayList<Double[]> dataSP = new ArrayList<Double[]>();
//                    dataSP.add(data.get(x));
//                    dataSP.add(data.get(y));
//                    spMatrix[x][y] = new ScatterPlot(dataSP, header.get(x), header.get(y), cellWidth, cellHeight, 5, model.getClassIDs());
//
//                    pnlSPLOM2.add(spMatrix[x][y]);
//                }
//            }
//        }
//    }
    public boolean isPointInsideSelection(Point2D p, int startX, int endX, int startY, int endY) {
        if (startX <= p.getX() && p.getX() <= endX
                && startY <= p.getY() && p.getY() <= endY) {
            return true;
        } else {
            return false;
        }
    }

    public int getDimIndexByName(String name) {
        int index = model.getHeaderIndex(name);
        return index;
    }

//    public void updateExplorationView(int c, int r) {
//        pnlScatterPlot.removeAll();
//
//        if (prevC >= 0 && prevR >= 0) {
//            spMatrix[prevC][prevR].setBorder(BorderFactory.createLineBorder(Color.black));
//        }
//        prevC = c;
//        prevR = r;
//
//        lensHistory.clear();
//
//        if (projection == null) {
//            projection = new ScatterPlot(spMatrix[c][r].getData(), spMatrix[c][r].getXDim(), spMatrix[c][r].getYDim(), pnlScatterPlot.getWidth(), pnlScatterPlot.getHeight(), 12, model.getClassIDs());
//        } else {
//            projection.changeDataSet(spMatrix[c][r].getDataPoints());
//        }
//        pnlScatterPlot.add(projection, BorderLayout.CENTER);
//        projection.updateUI();
//        pnlScatterPlot.updateUI();
//    }
    public void setColorCode(boolean showColorCode) {
        projection.setColorCode(showColorCode);
    }

    public int getExplorationPnlWidth() {
        return pnlOverlaySP.getWidth();
    }

    public int getExplorationPnlHeight() {
        return pnlOverlaySP.getHeight();
    }

    public void toggleEyetracking() {
        boolean newState = !eyeTribeCursor.isEnabled();
        eyeTribeCursor.setEnabled(newState);
        eyeTrackingGrid.setVisible(newState);
    }

    /**
     * Eye Tracking Stuff *
     */
    public EyeTribeCursor getEyeTribeCursor() {
        return this.eyeTribeCursor;
    }

    public void setLensEyePosition(int eyeX, int eyeY) {
        if (useEyePosition) {
            useEyePosition = false;
        }
    }

    public void updateDuration(int prevrow, int prevcolumn, double duration) {
        if (pnlSPLOM2.getComponentCount() > 0) {
            eyeTrackingGrid.setTotalDuration(prevcolumn, prevrow, duration);
        }
    }

    public void updateFixation(int row, int column) {
//        int splomColumn = bb.getSelectedColumn() + column;
//        int splomRow = bb.getSelectedRow() + row;
//
//        if (pnlSPLOM.getComponentCount() > 0) {
//            if (splomRow != splomColumn) {
//                spMatrix[splomRow][splomColumn].addFixation();
//            }
//        }
    }

    public void logInfo(int row, int col) {
        System.out.println("row: " + row + " col: " + col);
    }

    public void unselectAOIs(int row, int column) {
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run()
//            {
//                if (pnlSPLOM.getComponentCount() > 0) {
//                    for (int i = 0; i < spMatrix.length; i++) {
//                        for (int j = 0; j < spMatrix[0].length; j++) {
//                            if (i != j) {
//                                LineBorder boder = (LineBorder) spMatrix[i][j].getBorder();
//                                if (boder != null) {
//                                    Color boderColor = boder.getLineColor();
//                                    if (boderColor != recommendationColor) {
//                                        spMatrix[i][j].setBorder(null);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        });
    }

    private void saveHeatmap() {
        // toggleHeatmap();

        BufferedImage image = imagePainter.getImage();
        BufferedImage image2 = projection.getScreenShot();

        //Create filechooser, that only lets you choose PNG images.
        JFileChooser fc = new JFileChooser(lastFolder);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                final String name = f.getName();
                return name.endsWith(".png");
            }

            @Override
            public String getDescription() {
                return "*.png";
            }
        });

        int result = fc.showDialog(MainWindow.this, "Save");

        if (result == JFileChooser.APPROVE_OPTION) {
            String filePath = fc.getSelectedFile().getAbsolutePath();

            if (!filePath.endsWith(".png")) {
                filePath += ".png";
            }

            File file = new File(filePath);

            try {
                ImageIO.write(image2, "png", file);
            } catch (IOException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this,
                        "Could not write to: : " + filePath,
                        "Error while saving",
                        JOptionPane.ERROR_MESSAGE);
            }

            JOptionPane.showMessageDialog(this,
                    "Heatmap saved to: " + filePath,
                    "Done Capturing",
                    JOptionPane.PLAIN_MESSAGE);

            lastFolder = filePath.substring(0, filePath.lastIndexOf('\\'));
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
            useEyePosition = true;
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox cxbxClassColor;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel lblFilter1;
    private javax.swing.JLabel lblRegTypes2;
    private javax.swing.JPanel pnlCenter;
    private javax.swing.JPanel pnlChart;
    protected javax.swing.JPanel pnlClassLabels;
    private javax.swing.JPanel pnlClasses1;
    private javax.swing.JPanel pnlEyeTracking;
    protected javax.swing.JPanel pnlEyetracking1;
    protected javax.swing.JPanel pnlEyetracking2;
    protected javax.swing.JPanel pnlEyetracking3;
    protected javax.swing.JPanel pnlEyetracking4;
    private javax.swing.JPanel pnlLoadData;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JLayeredPane pnlOverlay;
    private javax.swing.JLayeredPane pnlOverlaySP;
    protected javax.swing.JPanel pnlSPLOM2;
    private javax.swing.JPanel pnlSettingA;
    private javax.swing.JPanel pnlSort;
    private javax.swing.JScrollPane scrClassLabels;
    // End of variables declaration//GEN-END:variables
}
