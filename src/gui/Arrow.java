/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.geom.Path2D;

/**
 *
 * @author Lin Shao
 */
public class Arrow extends Path2D.Double{
    public Arrow() {
            moveTo(0, 25);
            lineTo(0, 55);
            lineTo(40, 55);
            lineTo(40, 80);
            lineTo(100, 40);
            lineTo(40, 0);
            lineTo(40, 25);
            lineTo(0, 25);
        }
}
