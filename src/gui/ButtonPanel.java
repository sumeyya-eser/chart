package gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Lin Shao
 */
public class ButtonPanel extends JPanel implements MouseListener{
    private MainWindow mainWindow;
    private boolean selected = false;
    
    public ButtonPanel(){
        super();
        addMouseListener(this);
    }
    
    public ButtonPanel(LayoutManager layout){
        super(layout);
        addMouseListener(this);
    }
    
    public boolean isSelected() { return selected; }

    public void setSelected(boolean selected) {
        this.selected = selected;
        showSelectedVisuals(selected);
    }
    
    public void showSelectedVisuals(boolean selected){
        if(selected) {
            setBackground(new Color(176, 196, 222));
        } else {
            setBackground(new Color(240, 240, 240));
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        setSelected(!isSelected()); // toggle button
        if(this.getParent() == mainWindow.pnlClassLabels) {
            mainWindow.filterForClass();
        } else if(e.getSource() == mainWindow.btnLoadData) {
            mainWindow.loadData();
        } else if(e.getSource() == mainWindow.pnlEyetracking1) {
            mainWindow.toggleEyetracking();
        } else if(e.getSource() == mainWindow.pnlEyetracking2) {
            mainWindow.toggleHeatmapVisibility();
        } else if(e.getSource() == mainWindow.pnlEyetracking3) {
            mainWindow.toggleHeatmapTracking();
        } else if(e.getSource() == mainWindow.pnlEyetracking4) {
            mainWindow.resetHeatmap();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(mainWindow == null) {
            Container container = this.getParent();
            mainWindow = (MainWindow) SwingUtilities.getRoot(container);
        }
        setBackground(new Color(204, 204, 255));
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(e.getSource() == mainWindow.btnLoadData) {
            setBackground(new Color(153,153,153));
        } else {
            showSelectedVisuals(isSelected());
        }
    }

    @Override
    public void mouseClicked(MouseEvent e){}

    @Override
    public void mousePressed(MouseEvent e){}
    
}
