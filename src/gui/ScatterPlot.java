package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import model.DataPoint;

/**
 *
 * @author Lin Shao
 */
public class ScatterPlot extends JPanel implements MouseListener {
    private ArrayList<Double[]> data;
    private ArrayList<DataPoint> points = new ArrayList<>();
    
    private int width;
    private int height;
    private int dotSize = 12;
    private String xDim;
    private String yDim;
    
    private ArrayList<Boolean> classFilter = new ArrayList<Boolean>();
    private ArrayList<Integer> classIDs = new ArrayList<Integer>();
    
    private boolean colorcoding = true;
    
    public ScatterPlot(ArrayList<Double[]> data, String x, String y, int width, int height, int dotsize, ArrayList<Integer> classIDs) {
        this.data = data;
        this.classIDs = classIDs;
        this.dotSize = dotsize;
        this.xDim = x;
        this.yDim = y;
        this.width = width - 10;
        this.height = height - 10;
        setBorder(BorderFactory.createLineBorder(Color.black));
        setBackground(Color.white);
        addMouseListener(this);
        
        for(int i = 0; i < data.get(0).length; i++) {
            if (data.get(0)[i] != null && data.get(1)[i] != null) {
                DataPoint p = new DataPoint(data.get(0)[i], data.get(1)[i]);
                points.add(p);
            }
        }   
        setToolTipText("");
    }
    
    public ArrayList<Point2D> getPointsSelected(Double bbStartX, Double bbStartY, Double bbEndX, Double bbEndY) {
        ArrayList<Point2D> selectedPoints = new ArrayList<Point2D>();

        Point2D start = translateScreenToPlotCoordinate(bbStartX.intValue(), bbStartY.intValue());
        Point2D end = translateScreenToPlotCoordinate(bbEndX.intValue(), bbEndY.intValue());

        for (DataPoint p : points) {
            if (p.getX() > start.getX() && p.getX() < end.getX() && p.getY() > end.getY() && p.getY() < start.getY()) {
                selectedPoints.add(new Point2D.Double(p.getX(), p.getY()));
            }
        }
        return selectedPoints;
    }
    
    public ArrayList<DataPoint> getDataPoints() {
        return points;
    }
    
    public ArrayList<Double[]> getData() {
        return data;
    }
    
    public void setDataPoint(int index, DataPoint element) {
        points.set(index, element);
    }
    
    public void changeDataSet(ArrayList<DataPoint> points) {
        this.points = points;
    }
    
    public void changeFilter(ArrayList<Boolean> classFilter) {
        this.classFilter = classFilter;
        repaint();
    }
    
    public void setColorCode(boolean bool) {
        colorcoding = bool;
        repaint();
    }
    
    public String getXDim() {
        return xDim;
    }
    
    public String getYDim() {
        return yDim;
    }
    
    public Point2D translateScreenToPlotCoordinate(int x, int y) {
        double scrX = (double) x / (double) width;
        double scrY = (1 - ((double) y / (double) height));

        return new Point2D.Double(scrX, scrY);
    }
    
    public Point2D translateValueToScreenCoordinate(double x, double y) {
        double scrX = x * (double) width;
        double scrY = (height - ((double) y * (double) height));

        return new Point2D.Double(scrX, scrY);
    }
    
    public BufferedImage getScreenShot() {
        BufferedImage bi = new BufferedImage(
            this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);
        this.paint(bi.getGraphics());
        return bi;
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;        
        g2.setColor(new Color(153,153,255, 150));
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        int pointOpac = 150;
        
        for (int i = 0; i < points.size(); i++) {
            if (!Double.isNaN(points.get(i).getX()) || !Double.isNaN(points.get(i).getY())) {
                if (classFilter.isEmpty() || classFilter.get(classIDs.get(i))) {
                    g2.setColor(new Color(153, 153, 255, pointOpac));
                    
                    if (colorcoding && !classIDs.isEmpty()) {
                        switch (classIDs.get(i)) {
                            case 0:
//                                color = new Color(153, 153, 255, pointOpac);
                                g2.setColor(new Color(153, 153, 255, pointOpac));
                                break;
                            case 1:
//                                color = new Color(255, 165, 79, pointOpac);
                                g2.setColor(new Color(255, 165, 79, pointOpac));
                                break;
                            case 2:
//                                color = new Color(152, 251, 152, pointOpac);
                                g2.setColor(new Color(152, 251, 152, pointOpac));
                                break;
                            case 3:
//                                color = new Color(240, 128, 128, pointOpac);
                                g2.setColor(new Color(240, 128, 128, pointOpac));
                                break;
                            case 4:
//                                color = new Color(255, 193, 193, pointOpac);
                                g2.setColor(new Color(255, 193, 193, pointOpac));
                                break;
                            case 5:
//                                color = new Color(0, 255, 255, pointOpac);
                                g2.setColor(new Color(0, 255, 255, pointOpac));
                                break;
                            default:
//                                color = new Color(153, 153, 255, pointOpac);
                                g2.setColor(new Color(153, 153, 255, pointOpac));
                                break;
                        }
                    }
                    points.get(i).draw(g2, width, height, dotSize);  
                }
            }
        }
    }
    
    @Override
    public String getToolTipText(MouseEvent event) {
        for (DataPoint p : points) {
            Point newPoint = new Point((int) (p.getX() * width), (int) (height - (p.getY() * height)) );
            
            if (newPoint.distance(event.getX(), event.getY()) < 10) {
                return p.toString();
            }
        }
        return null;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Component container = (Component) this.getParent();
        MainWindow parent = (MainWindow) SwingUtilities.getRoot(container);
        
        if(this.getParent() == parent.pnlSPLOM2){
            int dim1 = parent.getDimIndexByName(this.xDim);
            int dim2 = parent.getDimIndexByName(this.yDim);
            //parent.updateExplorationView(dim1, dim2);
            
            this.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mousePressed(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}
}
