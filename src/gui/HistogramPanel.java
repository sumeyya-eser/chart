/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author seser
 */

//THIS CLASS IS NOT USED 
//bar chart is in the MainWindow
import java.awt.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

public class HistogramPanel extends JPanel implements MouseListener, MouseMotionListener {
 
	private int histogramHeight = 200;
	private int barWidth = 30;
	private int barGap = 0;

	private JPanel barPanel;
	private JPanel labelPanel;

	private List<Bar> bars = new ArrayList<Bar>();
	
	public HistogramPanel() {

		setBorder(new EmptyBorder(10, 10, 10, 10));
		setLayout(new BorderLayout());

		barPanel = new JPanel(new GridLayout(1, 0, barGap, 0));
		Border outer = new MatteBorder(1, 1, 1, 1, Color.BLACK);
		Border inner = new EmptyBorder(10, 10, 0, 10);
		Border compound = new CompoundBorder(outer, inner);
		barPanel.setBorder(compound);

		labelPanel = new JPanel(new GridLayout(1, 0, barGap, 0));
		labelPanel.setBorder(new EmptyBorder(5, 10, 0, 10));

		add(barPanel, BorderLayout.CENTER);
		add(labelPanel, BorderLayout.PAGE_END);

	}

	public void addHistogramColumn(String label, int value, Color color) {
		Bar bar = new Bar(label, value, color);
		bars.add(bar);
		
        

	}

	public void layoutHistogram() {
		barPanel.removeAll();
		labelPanel.removeAll();

		int maxValue = 0;

		for (Bar bar : bars)
			maxValue = Math.max(maxValue, bar.getValue());

		for (Bar bar : bars) {
			JLabel label = new JLabel(bar.getValue() + "");
			
			
			label.setHorizontalTextPosition(JLabel.CENTER);
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setVerticalTextPosition(JLabel.TOP);
			label.setVerticalAlignment(JLabel.BOTTOM);

			
			MouseAdapter listener = new DragMouseAdapter();
			label.addMouseListener(listener);
			label.setTransferHandler(new TransferHandler("icon"));
			
			
			int barHeight = (bar.getValue() * histogramHeight) / maxValue;
			Icon icon = new ColorIcon(bar.getColor(), barWidth, barHeight);
			label.setIcon(icon);
			barPanel.add(label);

			JLabel barLabel = new JLabel(bar.getLabel());
			barLabel.setHorizontalAlignment(JLabel.CENTER);
			labelPanel.add(barLabel);

		}
	}
	

	private class Bar {
		private String label;
		private int value;
		private Color color;

		public Bar(String label, int value, Color color) {
			this.label = label;
			this.value = value;
			this.color = color;
			

		}

		public String getLabel() {
			return label;
		}

		public int getValue() {
			return value;
		}

		public Color getColor() {
			return color;
		}
	
	}
	


	private class ColorIcon implements Icon {
		private int shadow = 3;

		private Color color;
		private int width;
		private int height;

		public ColorIcon(Color color, int width, int height) {
			this.color = color;
			this.width = width;
			this.height = height;
		}

		public int getIconWidth() {
			return width;
		}

		public int getIconHeight() {
			return height;
		}

		public void paintIcon(Component c, Graphics g, int x, int y) {
			g.setColor(color);
			g.fillRect(x, y, width - shadow, height);
			g.setColor(Color.GRAY);
			g.fillRect(x + width - shadow, y + shadow, shadow, height - shadow);
		}
	}

	private class DragMouseAdapter extends MouseAdapter {

		public void mousePressed(MouseEvent e) {

			JComponent c = (JComponent) e.getSource();
			TransferHandler handler = c.getTransferHandler();
			handler.exportAsDrag(c, e, TransferHandler.COPY);
		}
	}

	/*
	 * private class ClickListener extends MouseAdapter {
	 * 
	 * public void mousePressed(MouseEvent e) { prevPt = e.getPoint(); }
	 * 
	 * }
	 */

	/*
	 * private class DragListener extends MouseMotionAdapter {
	 * 
	 * public void mouseDragged(MouseEvent e) {
	 * 
	 * Point currentPt = e.getPoint(); barCorner.translate((int) (currentPt.getX() -
	 * prevPt.getX()), (int) (currentPt.getY() - prevPt.getY())); prevPt=currentPt;
	 * repaint();
	 * 
	 * } }
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
	
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}