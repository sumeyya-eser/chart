/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import org.jfree.ui.Drawable;

/**
 *
 * @author Lin Shao
 */
public class DataPoint extends Point implements Drawable{

    double x;
    double y; 
    
    public DataPoint (double x, double y){
        super();
        this.x = x;
        this.y = y;
        
    }
    
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    
    public void setpoint(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    @Override
    public String toString(){
        return "x: " +x+ " y: " +y;
    }
    
    public void draw(Graphics gd, int width, int height, int size)
    {
        gd.fillOval((int) (x * width), (int) (height - (y * height)), size, size);
    }

  //  @Override
    public void draw(Graphics2D gd, Rectangle2D rd)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
