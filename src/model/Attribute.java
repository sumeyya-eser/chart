package model;

/**
 *
 * @author Lin Shao
 */
public class Attribute {
    
    private String mesure;
    private String indicator;
    private String unit;
    private double value;
    private boolean estimated;

    public Attribute(String mesure, String indicator, String unit, double value, boolean estimated)
    {
        this.mesure = mesure;
        this.indicator = indicator;
        this.unit = unit;
        this.value = value;
        this.estimated = estimated;
    }

    public String getMesure()
    {
        return mesure;
    }

    public String getIndicator()
    {
        return indicator;
    }

    public String getUnit()
    {
        return unit;
    }

    public double getValue()
    {
        return value;
    }

    public boolean isEstimated()
    {
        return estimated;
    }
    
    
}
