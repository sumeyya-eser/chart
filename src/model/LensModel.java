/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import gui.MainWindow;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import model.CubicPoint;
import model.RegressionParts;

/**
 *
 * @author Lin Shao
 */
public class LensModel extends JPanel implements MouseListener{
    
    private int squareX;
    private int squareY;
    private int squareW;
    private int squareH;
    
    private int imageSizeX = 150;
    private int imageSizeY = 150;
    
    private ArrayList<Point2D> subset;
    private ArrayList<Point> linearReg;
    private ArrayList<Point> linearRegInv;
    private ArrayList<Point2D> linearRegsColor;
    private ArrayList<Double> linearRegsError;
    private ArrayList<Point2D> linearRegsColorInv;
    private ArrayList<Double> linearRegsErrorInv;  
    
    private ArrayList<Point2D> quadReg;
    private ArrayList<Point2D> quadRegInv;
    private ArrayList<Point2D> quadRegsColor;
    private ArrayList<Double> quadRegsError; 
    private ArrayList<Point2D> quadRegsColorInv;
    private ArrayList<Double> quadRegsErrorInv; 
    
    private ArrayList<CubicPoint> cubReg;
    private ArrayList<CubicPoint> cubRegInv;
    private ArrayList<CubicPoint> d4Reg;
    private ArrayList<CubicPoint> d4RegInv;

    public ArrayList<CubicPoint> getCubReg()
    {
        return cubReg;
    }

    public void setCubReg(ArrayList<CubicPoint> cubReg)
    {
        this.cubReg = cubReg;
    }

    public ArrayList<CubicPoint> getCubRegInv()
    {
        return cubRegInv;
    }

    public void setCubRegInv(ArrayList<CubicPoint> cubRegInv)
    {
        this.cubRegInv = cubRegInv;
    }

    public ArrayList<CubicPoint> getD4Reg()
    {
        return d4Reg;
    }

    public void setD4Reg(ArrayList<CubicPoint> d4Reg)
    {
        this.d4Reg = d4Reg;
    }

    public ArrayList<CubicPoint> getD4RegInv()
    {
        return d4RegInv;
    }

    public void setD4RegInv(ArrayList<CubicPoint> d4RegInv)
    {
        this.d4RegInv = d4RegInv;
    }
    
    private ArrayList<Point2D> selectedPoints;
    private Color[] colorError;
    private boolean selected = false;

    public LensModel(int squareX, int squareY, int squareW, int squareH, ArrayList<Point2D> subset, ArrayList<Point> linearReg, ArrayList<Point> linearRegInv, ArrayList<Point2D> linearRegsColor, 
            ArrayList<Double> linearRegsError, ArrayList<Point2D> linearRegsColorInv, ArrayList<Double> linearRegsErrorInv, ArrayList<Point2D> quadReg, ArrayList<Point2D> quadRegInv, ArrayList<Point2D> quadRegsColor, 
            ArrayList<Double> quadRegsError, ArrayList<Point2D> quadRegsColorInv, ArrayList<Double> quadRegsErrorInv, ArrayList<CubicPoint> cubRegs, ArrayList<CubicPoint> cubRegsInv, ArrayList<CubicPoint> d4Regs, ArrayList<CubicPoint> d4RegsInv, 
            ArrayList<Point2D> selectedPoints, Color[] colorError)
    {
        this.squareX = squareX;
        this.squareY = squareY;
        this.squareW = squareW;
        this.squareH = squareH;
        this.subset = subset;
        this.linearReg = linearReg;
        this.linearRegInv = linearRegInv;
        this.linearRegsColor = linearRegsColor;
        this.linearRegsError = linearRegsError;
        this.linearRegsColorInv = linearRegsColorInv;
        this.linearRegsErrorInv = linearRegsErrorInv;
        this.quadReg = quadReg;
        this.quadRegInv = quadRegInv;
        this.quadRegsColor = quadRegsColor;
        this.quadRegsError = quadRegsError;
        this.quadRegsColorInv = quadRegsColorInv;
        this.quadRegsErrorInv = quadRegsErrorInv;
        this.cubReg = cubRegs;
        this.cubRegInv = cubRegsInv;
        this.d4Reg = d4Regs;
        this.d4RegInv = d4RegsInv;
        this.selectedPoints = selectedPoints;
        this.colorError = colorError;
        
        this.setPreferredSize(new Dimension(imageSizeX, imageSizeY));
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        this.addMouseListener(this);
    }
    
    protected void paintComponent(Graphics g) {
        
//        g.fillRect(10, 10, 20, 20);
        
        Graphics2D g2 = (Graphics2D) g;
        
//        g2.setBackground(Color.WHITE);
        g2.setStroke(new BasicStroke(1));
        
        if (linearReg.size() > 0) {
            g2.setColor(new Color(0, 183, 255));
            ArrayList<Point2D> normalize = normalize(linearReg);
            g2.drawLine((int) normalize.get(0).getX(), (int) normalize.get(0).getY(), (int) normalize.get(1).getX(), (int) normalize.get(1).getY());
        }

        if (linearRegInv.size() > 0) {
            g2.setColor(Color.red);
            ArrayList<Point2D> normalize = normalize(linearRegInv);
            g2.drawLine((int) normalize.get(0).getX(), (int) normalize.get(0).getY(), (int) normalize.get(1).getX(), (int) normalize.get(1).getY());
        }
        
        if (linearRegsColor.size() > 0) {
            ArrayList<Point2D> normalizeP = normalize2D(linearRegsColor);
            drawLinearColor(g2, normalizeP, linearRegsError);
        }

        if (linearRegsColorInv.size() > 0) {
            ArrayList<Point2D> normalizeP = normalize2D(linearRegsColorInv);
            drawLinearColor(g2, normalizeP, linearRegsErrorInv);
        }

        if (quadReg.size() > 0) {
            g2.setColor(new Color(0, 183, 255));
            ArrayList<Point2D> normalize = normalize2D(quadReg);
            drawQuadModel(g2, normalize);
        }

        if (quadRegInv.size() > 0) {
            g2.setColor(Color.red);
            ArrayList<Point2D> normalize = normalize2D(quadRegInv);
            drawQuadModel(g2, normalize);
        }

        if (quadRegsColor.size() > 0) {
            ArrayList<Point2D> normalizeP = normalize2D(quadRegsColor);
            drawQuadColor(g2, normalizeP, quadRegsError);
        }

        if (quadRegsColorInv.size() > 0) {
            ArrayList<Point2D> normalizeP = normalize2D(quadRegsColorInv);
            drawQuadColor(g2, normalizeP, quadRegsErrorInv);
        }
        
//        if (cubReg.size() > 0) {
//            g2.setColor(new Color(0, 183, 255));
//            ArrayList<Point2D> pointsCub = new ArrayList<Point2D>();
//            for(CubicPoint p : cubReg){
//                pointsCub.add(p.getPoint());
//            }
//            ArrayList<Point2D> normalize = normalize2D(pointsCub);
//            drawQuadModel(g2, normalize);
//        }
//
//        if (cubRegInv.size() > 0) {
//            g2.setColor(Color.red);
//            ArrayList<Point2D> pointsCub = new ArrayList<Point2D>();
//            for(CubicPoint p : cubRegInv){
//                pointsCub.add(p.getPoint());
//            }
//            ArrayList<Point2D> normalize = normalize2D(pointsCub);
//            drawQuadModel(g2, normalize);
//        }
        
        if(cubReg.size() > 0) {
            g2.setColor(new Color(0, 183, 255));
            ArrayList<CubicPoint> pointsCub = new ArrayList<CubicPoint>();
            
            for(CubicPoint p : cubReg){
                CubicPoint newPoint = new CubicPoint(normalize2DPoint(p.getPoint()), selected);
                newPoint.setDistance(p.getDistance());
                pointsCub.add(newPoint);
            }
            
            drawPolynomialModel(g2, pointsCub, new Color(0, 183, 255), 255);
        }
            
        if (cubRegInv.size() > 0) {
            g2.setColor(Color.red);
//            ArrayList<Point2D> pointsCub = new ArrayList<Point2D>();
//            for(CubicPoint p : cubRegInv){
//                pointsCub.add(p.getPoint());
//            }
            drawPolynomialModel(g2, cubRegInv, Color.red, 255);
        }
        
        if (d4Reg.size() > 0) {
            g2.setColor(new Color(0, 183, 255));
            ArrayList<Point2D> pointsCub = new ArrayList<Point2D>();
            for(CubicPoint p : d4Reg){
                pointsCub.add(p.getPoint());
            }
            ArrayList<Point2D> normalize = normalize2D(pointsCub);
            drawQuadModel(g2, normalize);
        }

        if (d4RegInv.size() > 0) {
            g2.setColor(Color.red);
            ArrayList<Point2D> pointsCub = new ArrayList<Point2D>();
            for(CubicPoint p : d4RegInv){
                pointsCub.add(p.getPoint());
            }
            ArrayList<Point2D> normalize = normalize2D(pointsCub);
            drawQuadModel(g2, normalize);
        }
    }
    
    
    public ArrayList<Point2D> normalize(ArrayList<Point> dataPoints){
        ArrayList<Point2D> normalized = new ArrayList<Point2D>();
        
        for(Point p : dataPoints){
            double xTmp  = (p.getX() - squareX) > 0 ? (p.getX() - squareX) : 0;
            double yTmp  = (p.getY() - squareY) > 0 ? (p.getY() - squareY) : 0;
            double xNorm = xTmp/squareW;
            double yNorm = yTmp/squareH;
            normalized.add(new Point2D.Double(xNorm * imageSizeX, yNorm * imageSizeY));
        }
        
        return normalized;
    }
    
    public ArrayList<Point2D> normalize2D(ArrayList<Point2D> dataPoints){
        ArrayList<Point2D> normalized = new ArrayList<Point2D>();
        
        for(Point2D p : dataPoints){
            double xTmp  = (p.getX() - squareX) > 0 ? (p.getX() - squareX) : 0;
            double yTmp  = (p.getY() - squareY) > 0 ? (p.getY() - squareY) : 0;
            double xNorm = xTmp/squareW;
            double yNorm = yTmp/squareH;
            normalized.add(new Point2D.Double(xNorm * imageSizeX, yNorm * imageSizeY));
        }
        
        return normalized;
    }
    
    public Point2D normalize2DPoint(Point2D p){
        Point2D normalized;
        
        double xTmp = (p.getX() - squareX) > 0 ? (p.getX() - squareX) : 0;
        double yTmp = (p.getY() - squareY) > 0 ? (p.getY() - squareY) : 0;
        double xNorm = xTmp / squareW;
        double yNorm = yTmp / squareH;
        normalized = new Point2D.Double(xNorm * imageSizeX, yNorm * imageSizeY);
        
        return normalized;
    }
    
    private void drawLinearColor(Graphics2D g2, ArrayList<Point2D> linearPoints, ArrayList<Double> linearPointsError)
    {        
        for (int i = 0; i < linearPoints.size() - 1; i++) {
            int colorIndex = (int) (linearPointsError.get(i) / 0.005);
            colorIndex = (colorIndex < 10) ? colorIndex : 9;
            g2.setColor(colorError[colorIndex]);
            g2.drawLine((int) linearPoints.get(i).getX(), (int) linearPoints.get(i).getY(), (int) linearPoints.get(i + 1).getX(), (int) linearPoints.get(i + 1).getY());
        }
    }
    
    public void drawQuadModel(Graphics2D g2, ArrayList<Point2D> quadPoints)
    {    
        if (!quadPoints.isEmpty()) {
            int nSize = quadPoints.size();
            int[] xPoints = new int[nSize];
            int[] yPoints = new int[nSize];
            for (int i = 0; i < quadPoints.size(); i++) {
                xPoints[i] = (int) quadPoints.get(i).getX();
                yPoints[i] = (int) quadPoints.get(i).getY();
            }
            g2.drawPolyline(xPoints, yPoints, nSize);
        }
    }
    
    public void drawQuadColor(Graphics2D g2, ArrayList<Point2D> quadPoints, ArrayList<Double> linearPointsError)
    {        
        for(int i= 0; i < quadPoints.size()-1; i++){
            int colorIndex = (int) (linearPointsError.get(i) / 0.005);
            colorIndex = (colorIndex < 10) ? colorIndex : 9;
            g2.setColor(colorError[colorIndex]);
            
            int[] xPoints = new int[2];
            int[] yPoints = new int[2];
            xPoints[0] = (int) quadPoints.get(i).getX();
            xPoints[1] = (int) quadPoints.get(i+1).getX();
            yPoints[0] = (int) quadPoints.get(i).getY();
            yPoints[1] = (int) quadPoints.get(i+1).getY();
            
            g2.drawPolyline(xPoints, yPoints, 2);
        }
    }
    
    public void drawPolynomialModel(Graphics2D g2, ArrayList<CubicPoint> points, Color lastColor, int saturation)
    {
        for(int i = 0; i < points.size()-1; i++){
            boolean colorMode = points.get(i).getColorMode();
            Color currColor = lastColor;
            double dist = points.get(i).getDistance();
            
            if (dist > 0) {
                System.out.println("dist=" + (int)(dist));
                int colorIndex = (int) (dist / 0.005);
                System.out.println("colorindex=" + colorIndex);
                colorIndex = (colorIndex < 10) ? colorIndex : 9;
                currColor = colorError[colorIndex];
            }
            
            if(colorMode == false){
                g2.setColor(new Color(currColor.getRed(), currColor.getGreen(), currColor.getBlue(), 25));
            }else{
                g2.setColor(new Color(currColor.getRed(), currColor.getGreen(), currColor.getBlue(), saturation));
            }
            
            int[] xPoints = new int[2];
            int[] yPoints = new int[2];
            xPoints[0] = (int) points.get(i).getX();
            xPoints[1] = (int) points.get(i+1).getX();
            yPoints[0] = (int) points.get(i).getY();
            yPoints[1] = (int) points.get(i+1).getY();
            
            g2.drawPolyline(xPoints, yPoints, 2);
        }
    }

    public int getSquareX()
    {
        return squareX;
    }

    public void setSquareX(int squareX)
    {
        this.squareX = squareX;
    }

    public int getSquareY()
    {
        return squareY;
    }

    public void setSquareY(int squareY)
    {
        this.squareY = squareY;
    }

    public int getSquareW()
    {
        return squareW;
    }

    public void setSquareW(int squareW)
    {
        this.squareW = squareW;
    }

    public int getSquareH()
    {
        return squareH;
    }

    public void setSquareH(int squareH)
    {
        this.squareH = squareH;
    }

    public ArrayList<Point2D> getSubset()
    {
        return subset;
    }

    public void setSubset(ArrayList<Point2D> subset)
    {
        this.subset = subset;
    }

    public ArrayList<Point> getLinearReg()
    {
        return linearReg;
    }

    public void setLinearReg(ArrayList<Point> linearReg)
    {
        this.linearReg = linearReg;
    }

    public ArrayList<Point> getLinearRegInv()
    {
        return linearRegInv;
    }

    public void setLinearRegInv(ArrayList<Point> linearRegInv)
    {
        this.linearRegInv = linearRegInv;
    }

    public ArrayList<Point2D> getLinearRegsColor()
    {
        return linearRegsColor;
    }

    public void setLinearRegsColor(ArrayList<Point2D> linearRegsColor)
    {
        this.linearRegsColor = linearRegsColor;
    }

    public ArrayList<Double> getLinearRegsError()
    {
        return linearRegsError;
    }

    public void setLinearRegsError(ArrayList<Double> linearRegsError)
    {
        this.linearRegsError = linearRegsError;
    }

    public ArrayList<Point2D> getLinearRegsColorInv()
    {
        return linearRegsColorInv;
    }

    public void setLinearRegsColorInv(ArrayList<Point2D> linearRegsColorInv)
    {
        this.linearRegsColorInv = linearRegsColorInv;
    }

    public ArrayList<Double> getLinearRegsErrorInv()
    {
        return linearRegsErrorInv;
    }

    public void setLinearRegsErrorInv(ArrayList<Double> linearRegsErrorInv)
    {
        this.linearRegsErrorInv = linearRegsErrorInv;
    }

    public ArrayList<Point2D> getQuadReg()
    {
        return quadReg;
    }

    public void setQuadReg(ArrayList<Point2D> quadReg)
    {
        this.quadReg = quadReg;
    }

    public ArrayList<Point2D> getQuadRegInv()
    {
        return quadRegInv;
    }

    public void setQuadRegInv(ArrayList<Point2D> quadRegInv)
    {
        this.quadRegInv = quadRegInv;
    }

    public ArrayList<Point2D> getQuadRegsColor()
    {
        return quadRegsColor;
    }

    public void setQuadRegsColor(ArrayList<Point2D> quadRegsColor)
    {
        this.quadRegsColor = quadRegsColor;
    }

    public ArrayList<Double> getQuadRegsError()
    {
        return quadRegsError;
    }

    public void setQuadRegsError(ArrayList<Double> quadRegsError)
    {
        this.quadRegsError = quadRegsError;
    }

    public ArrayList<Point2D> getQuadRegsColorInv()
    {
        return quadRegsColorInv;
    }

    public void setQuadRegsColorInv(ArrayList<Point2D> quadRegsColorInv)
    {
        this.quadRegsColorInv = quadRegsColorInv;
    }

    public ArrayList<Double> getQuadRegsErrorInv()
    {
        return quadRegsErrorInv;
    }

    public void setQuadRegsErrorInv(ArrayList<Double> quadRegsErrorInv)
    {
        this.quadRegsErrorInv = quadRegsErrorInv;
    }

    public ArrayList<Point2D> getSelectedPoints()
    {
        return selectedPoints;
    }

    public void setSelectedPoints(ArrayList<Point2D> selectedPoints)
    {
        this.selectedPoints = selectedPoints;
    }

    public boolean isSelected(){
        return selected;
    }
    @Override
    public void mouseClicked(MouseEvent e)
    {
        if(!selected){
            this.setBorder(BorderFactory.createLineBorder(Color.CYAN));
            selected = true;
        }else{
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            selected = false;
        }
        this.repaint();
        
        Component container = (Component) this.getParent();
        MainWindow parent = (MainWindow) SwingUtilities.getRoot(container);
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
        
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    
}
