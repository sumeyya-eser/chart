package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lin Shao
 */
public class FileModel {

    private String delimiter;
    private int nrOfAttributes;
    private ArrayList<String> dataHeader = new ArrayList<String>();;
    private ArrayList<Object>[] dataSet;
    private ArrayList<Double[]> minMaxValues;
    private ArrayList<Integer> classIndex;
    private int nrOfCatDim;
    private ArrayList<String> classNames = new ArrayList<String>();
    private ArrayList<Integer> classIDs = new ArrayList<Integer>();
    
    private ArrayList<String> countries = new ArrayList<String>();
    private ArrayList<Record> data = new ArrayList<>();

    public FileModel(String path, String delimiter, boolean normalize) {
        
        try {
            this.delimiter = delimiter;
            
            BufferedReader in = new BufferedReader(new FileReader(path));
            readHeader(in.readLine());
            readData(in);
            if(normalize) dataSet = normalizeData(dataSet);
            
        } catch (IOException ex) {
            Logger.getLogger(FileModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public FileModel(String path, boolean normalize) {
        
        try {
            this.delimiter = ",";
            
            BufferedReader in = new BufferedReader(new FileReader(path));
            in.readLine();
            preprocess(in);
            nrOfAttributes = dataHeader.size();
            dataSet = getDataset();
            if(normalize) dataSet = normalizeData(dataSet);
            
        } catch (IOException ex) {
            Logger.getLogger(FileModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private ArrayList<Object>[] getDataset(){
        ArrayList<Object>[] d = new ArrayList[dataHeader.size()];
        
        minMaxValues = new ArrayList<>();
        for (int i = 0; i < dataHeader.size(); i++) {
            d[i] = new ArrayList<>();
            Double[] minMax = {Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
            minMaxValues.add(minMax);
        }
        
        for(int i = 0; i < data.size(); i++){
            ArrayList<Object> recCountry = data.get(i).getDimensionsObj();
            
            for(int j = 0; j < dataHeader.size(); j++){
                if(d[j] == null) d[j] = new ArrayList<>();
                if(recCountry.get(j) != null) {
                    double value = (double) recCountry.get(j);
                    d[j].add(value);                      
                    minMaxValues.get(j)[0] = (minMaxValues.get(j)[0] > value) ? value : minMaxValues.get(j)[0];     //min
                    minMaxValues.get(j)[1] = (minMaxValues.get(j)[1] < value) ? value : minMaxValues.get(j)[1];     //max
                }else{
                    d[j].add(null);
                }
            }
        }
        
        return d;
    }

    private void preprocess(BufferedReader in){
        try {
            String next;
            String values[];
                        
            int index = 0;
            while ((next = in.readLine()) != null) {
                values = next.split(delimiter);
                String location = values[0].substring(1, values[0].length()-1);
                String country = values[1].substring(1, values[1].length()-1);
                
                String messure = values[3].substring(1, values[3].length()-1);
                String indicator = values[2].substring(1, values[2].length()-1);
                String type = values[6].substring(1, values[6].length()-1);
                String unit = values[9].substring(1, values[9].length()-1);
                double value = Double.parseDouble(values[14]);//.substring(1, values[14].length()-1);
                
                if (type.equals("TOT")) {
                    if (!dataHeader.contains(indicator)) {
                        dataHeader.add(indicator);
                    }
                    Attribute a = new Attribute(messure, indicator, unit, value, false);
                    int idAttribute = dataHeader.indexOf(indicator);
//                    int idAttribute = dataHeader.indexOf(indicator + "-" + type);

                    if (!countries.contains(location)) {
                        countries.add(location);
                        data.add(new Record(index, location, country));

                        data.get(index).addAttribute(idAttribute, a);
                        index++;
                    } else {
                        int id = getID(location);
                        data.get(id).addAttribute(idAttribute, a);
                    }
                }
            }            
        } catch (IOException ex) {
            Logger.getLogger(FileModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void readHeader(String in) {
        String[] header = in.split(delimiter);
        
        dataHeader = new ArrayList<String>();
        dataSet = new ArrayList[header.length];
        classIndex = new ArrayList<Integer>();
        
        for (int i = 0; i < header.length; i++) {
            String firstChar = String.valueOf(header[i].charAt(0));
            if(firstChar.equals(" ")){
                header[i] = header[i].substring(1, header[i].length());
            }
            if(header[i].contains("Class") || header[i].contains("class") || header[i].contains("(c)") || header[i].contains("(C)")){
                nrOfCatDim++;
                classIndex.add(i);
            }
            this.dataHeader.add(header[i]);
            this.dataSet[i] = new ArrayList<Object>();
        }
        nrOfAttributes = header.length - nrOfCatDim;
    }

    private void readData(BufferedReader in){
        try {
            String next;
            String values[];
            
            minMaxValues = new ArrayList<>();
            for (int i = 0; i < dataHeader.size(); i++) {
                Double[] minMax = {Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY};
                minMaxValues.add(minMax);
            }
            
            while ((next = in.readLine()) != null) {
                values = next.split(delimiter);
                for (int i = 0; i < values.length; i++) {
                    if (values[i].length() > 0 && !values[i].contains("?")) {    // && values[i].matches("([0-9]*)\\\\.[0]")
                        if(isNumeric(values[i])) {
                            double value = Double.parseDouble(values[i]);
                            dataSet[i].add(value);

                            minMaxValues.get(i)[0] = (minMaxValues.get(i)[0] > value) ? value : minMaxValues.get(i)[0];     //min
                            minMaxValues.get(i)[1] = (minMaxValues.get(i)[1] < value) ? value : minMaxValues.get(i)[1];     //max
                        }
                        else{
                            dataSet[i].add(values[i]);
                        }
                    }
                    else{
                        dataSet[i].add(null);
                    }
                }
                if (values.length < dataHeader.size()) {
                    for (int e = values.length; e < dataHeader.size(); e++) {
                        dataSet[e].add(null);
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ArrayList<Object>[] normalizeData(ArrayList<Object>[] data) {
        ArrayList<Object>[] normalizedData = new ArrayList[data.length];

        for (int i = 0; i < data.length; i++) {
            normalizedData[i] = new ArrayList<Object>();
            for (int j = 0; j < data[i].size(); j++) {

                if (data[i].get(j) != null) {
                    if (isNumeric(data[i].get(j).toString())) {
                        double val = (((Double) data[i].get(j)) - minMaxValues.get(i)[0]) / (minMaxValues.get(i)[1] - minMaxValues.get(i)[0]);
                        normalizedData[i].add(val);
                    } else {
                        // string
                        normalizedData[i].add(data[i].get(j));
                    }
                } else {
                    // null
                    normalizedData[i].add(data[i].get(j));
                }
            }
        }
        return normalizedData;
    }


    /**
     * For unequal dimension length
     *
     * @param x
     * @param y
     * @return length of the smaller dimension
     */
    public int getCombinationLength(int x, int y) {
        int length = dataSet[x].size();
        if (dataSet[x].size() > dataSet[y].size()) {
            length = dataSet[y].size();
        }
        return length;
    }

    public String getDelimiter() {
        return delimiter;
    }

    public int getNrOfAttributes() {
        return nrOfAttributes;
    }
    
    public int getNrOfCatDims(){
        return nrOfCatDim;
    }
    
    public ArrayList<Integer> getClassIndex(){
        return classIndex;
    }

    public ArrayList<String> getDataHeader() {
        return dataHeader;
    }
    
    public String getHeaderAt(int index){
        return dataHeader.get(index);
    }
    
    public int getHeaderIndex(String name){
        for(int i = 0; i < dataHeader.size(); i ++){
            if(name.equals(dataHeader.get(i)) ){
                return i;
            }
        }
        return -1;
    }

    public ArrayList<Object>[] getDataSet() {
        return dataSet;
    }
    
    public ArrayList<Double[]> getNumDataSet() {
        ArrayList<Double[]> numData = new ArrayList<>();
        
        for(int i = 0; i < dataSet.length - nrOfCatDim; i++)
        {
            Double[] dim = new Double[dataSet[i].size()];
            for(int j = 0; j < dataSet[i].size(); j++){
                dim[j] = (Double) dataSet[i].get(j);
            }
            numData.add(dim);
        }
        
        return numData;
    }
    
    private int getID(String name){
        int id = 0;
        for(String s : countries){
            if(s.equals(name)){
                return id;
            }
            id++;
        }
        
        return -1;
    }
    
    public ArrayList<String> getCountryLabels(){
        return countries;
    }
    
    public ArrayList<Object> getDataSet(int index){
        return dataSet[index];
    }
    
    public ArrayList<String> getClassNames()
    {
        return classNames;
    }

    public void setClassNames(ArrayList<String> classNames)
    {
        this.classNames = classNames;
    }
    
    public ArrayList<Integer> getClassIDs()
    {
        return classIDs;
    }
    
    public void setClasses2Points(ArrayList<Integer> classIds){
        this.classIDs = classIds;
    }
    
    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
    
}
