/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Lin Shao
 */
public class Record {
    
    private int index;
    private String location;
    private String Country;
    private ArrayList<Attribute> dimensions = new ArrayList<>();;

    public Record(int index, String location, String Country)
    {
        this.index = index;
        this.location = location;
        this.Country = Country;
    }
    
    public void addAttribute(Attribute a){
        dimensions.add(a);
    }
    
    public void addAttribute(int index, Attribute a){
        if(dimensions.size() < index){
            for(int i = dimensions.size(); i < index; i++){
                dimensions.add(i, null);
            }
        }
        dimensions.add(index, a);
        
    }
    
    public ArrayList<Attribute> getDimensions(){
        return dimensions;
    }
    
    public ArrayList<Object> getDimensionsObj(){
        ArrayList<Object> d = new ArrayList<>();
        for(Attribute a : dimensions){
            if(a != null){
                d.add(a.getValue());
            }else{
                d.add(null);
            }
        }
        
        return d;
    }
}
