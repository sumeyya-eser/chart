package model;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Lin Shao
 */
public class Utils {
    
    public static ArrayList<Point2D>[] splittDataset(ArrayList<Point2D> points)
    {
        ArrayList<Point2D>[] splittedData = new ArrayList[2];
        ArrayList<Point2D> subsetA = new ArrayList<Point2D>();
        ArrayList<Point2D> subsetB = new ArrayList<Point2D>();
        ArrayList<Point2D> tmpPoints = new ArrayList<Point2D>(points);

        int nrOfpoints = points.size();

        //systematic sampling
        for (int i = 0; i < nrOfpoints; i++) {
            if (i % 2 == 0) {
                subsetA.add(tmpPoints.get(i));
            } else {
                subsetB.add(tmpPoints.get(i));
            }
        }

        splittedData[0] = subsetA;
        splittedData[1] = subsetB;

        return splittedData;
    }
    
    public static double calculateMeanPercistenceEqual(ArrayList<Double> binHeights)
    {
        double d = 0;
        double meanBin =  1.0 / binHeights.size();
        
        for (int i = 0; i < binHeights.size(); i++) 
        {
            d += (binHeights.get(i) - meanBin)*(binHeights.get(i) - meanBin) / meanBin;
        }
        
        return d;
    }
     
    public static double calculateMeanPercistenceEqual2(ArrayList<Double> binHeights)
    {
        double d = 0;
        double meanBin =  1.0 / binHeights.size();
        
        for (int i = 0; i < binHeights.size(); i++) 
        {
            d += Math.abs(binHeights.get(i) - meanBin);
        }
        
        return d;
    }
    
    public static double findNextPoint(double midX, double midY, ArrayList<Point2D> selectedPoints)
    {
        double dist = Double.POSITIVE_INFINITY;
        for (Point2D p : selectedPoints) {
            double distX = Math.abs(p.getX() - midX);
            double distY = Math.abs(p.getY() - midY);

            double distTmp = Math.sqrt(distY * distY + distX * distX);
            dist = (distTmp < dist) ? distTmp : dist;
        }
        return dist;
    }
    
    public static int findSmallestError(double[] errorValues){
        int smallestIndex = -1;
        double smallestEr = 100.0;
        
        for(int i = 0; i < errorValues.length; i++){
            if(errorValues[i] < smallestEr){
                 smallestEr = errorValues[i];
                 smallestIndex = i;
            }
        }
        return smallestIndex;
    }

    /**
     * Sort HashMaps
     *
     * @param <K>
     * @param <V>
     * @param map
     * @return sorted HashMap
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map)
    {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    public static ArrayList<Pattern> getAllClusterPatternsByIndex(String x, String y, ArrayList<Pattern> patternList)
    {
        ArrayList<Pattern> result = new ArrayList<Pattern>();
        for (Pattern p : patternList) {
            String dimX = p.getxDimension();
            String dimY = p.getyDimension();
            if (dimX.equals(x) && dimY.equals(y)) {
                result.add(p);
            }
        }
        return result;
    }

    public static ArrayList<Point2D[]> getAllClusterPatternsPoints(ArrayList<Pattern> list, FileModel model)
    {
        ArrayList<Point2D[]> localPatternsArray = new ArrayList<Point2D[]>();
        ArrayList<String> labels = new ArrayList<String>();
        ArrayList<Integer> classIDs = new ArrayList<Integer>();
        
        if(model == null || model.getNrOfCatDims() == 0){
            for (Pattern p : list) {
                ArrayList<Point2D> listPoints = p.getPoints();
                Point2D[] arrayPoints = listPoints.toArray(new Point2D[listPoints.size()]);
                localPatternsArray.add(arrayPoints);
            }
        
        }else {
            ArrayList<Object>[] data = model.getDataSet();
            int classIndex = model.getClassIndex().get(0);      // 0 -> only 1 class label
            
            for(int i = 0; i < data[classIndex].size(); i++){
                
                if(!labels.contains(data[classIndex].get(i))){
                    labels.add(String.valueOf(data[classIndex].get(i)));
                }
                int index = labels.indexOf(data[classIndex].get(i));
                classIDs.add(index);
            }
            
            model.setClassNames(labels);
            model.setClasses2Points(classIDs);
            ArrayList<Point2D> points = list.get(0).getPoints();
            
            for(String className : labels){
                
                ArrayList<Point2D> pointsOfclass = new ArrayList<Point2D>();
                for(int i = 0; i < points.size(); i++)
                {
                    if(data[classIndex].get(i).equals(className)){
                        pointsOfclass.add(points.get(i));
                    }
                }
                Point2D[] arrayPoints = pointsOfclass.toArray(new Point2D[pointsOfclass.size()]);
                localPatternsArray.add(arrayPoints);
            }
        }
        return localPatternsArray;
    }

    public static ArrayList<String> getAllClusterPatternsIDs(ArrayList<Pattern> list, FileModel model)
    {
        ArrayList<String> result = new ArrayList<String>();
        if(model.getNrOfCatDims() > 0)
        {
            ArrayList<String> classNames = model.getClassNames();
            
            for(String name : classNames){
                result.add(name);
            }
            
        }else {
            for (Pattern p : list) {
                result.add(p.getId());
            }
        }
        return result;
    }
}
