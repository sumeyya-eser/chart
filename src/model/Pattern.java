/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Shao
 */
public class Pattern {
    
    private String id;
    private String xDimension;
    private String yDimension;
    private Point globalPos;
    private BufferedImage spImage;
    private ArrayList<Point2D> points = new ArrayList<>();
    
    
    
    public Pattern(String id, ArrayList<Point2D> points){
        this.id = id;
        this.points = points;
    }
    
    public Pattern(String id, ArrayList<Point2D> points, String dimCombination){
        this.id = id;
        this.points = points;
        
        this.xDimension = dimCombination.substring(0, dimCombination.indexOf("#"));         //bug!!!:  = raster:pgb-derived.Temperature
        this.yDimension = dimCombination.substring(dimCombination.indexOf("#")+1);
    }
    
    public String getId() {
        return id;
    }

    public String getxDimension() {
        return xDimension;
    }
    
    public String getyDimension() {
        return yDimension;
    }

    public Point getGlobalPos() {
        return globalPos;
    }

    public BufferedImage getSpImage() {
        return spImage;
    }

    public ArrayList<Point2D> getPoints() {
        return points;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setxDimension(String xDimension) {
        this.xDimension = xDimension;
    }

    public void setyDimension(String yDimension) {
        this.yDimension = yDimension;
    }

    public void setGlobalPos(Point globalPos) {
        this.globalPos = globalPos;
    }

    public void setSpImage(BufferedImage spImage) {
        this.spImage = spImage;
    }

    public void setPoints(ArrayList<Point2D> points) {
        this.points = points;
    }
    
}
